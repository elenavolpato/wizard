/*
Challenge
1. Create a function called getDiceRollArray that uses a 
   for loop to return an array of random numbers between 1-6. 
2  The function should have diceCount as a parameter and the 
   array it returns should be diceCount length.
3  For testing purposes, call the function with a diceCount of 
   3 and log out the result. 
** check out hint.md for extra help! **
*/


const players = [{
   elementId: "hero",
   name: "Wizard",
   avatar: "images/wizard.png",
   health: 60,
   diceRoll: [],
   diceCount: 3
},
 {
   elementId: "monster",
   name: "Orc",
   avatar: "images/orc.png",
   health: 10,
   diceRoll: [],
   diceCount: 1
}]

function getDiceRollArray(diceCount) {
   return new Array(diceCount).fill(0).map(function () {
       return Math.floor(Math.random() * 6) + 1
       })
}

function getDiceHtml(diceCount){
   return getDiceRollArray(diceCount).map(function(diceNum){
       return `<div class="dice">${diceNum}</div>`
   }).join('')   
}

function renderCharacters(players) {
   for (let player of players) {
      const { elementId, name, avatar, health, diceRoll, diceCount } = player
      const diceHtml = getDiceHtml(diceCount)
      document.getElementById(elementId).innerHTML =
         `<div class="character-card">
            <h4 class=${name}> Wizard </h4>
            <img class="avatar" src="${avatar}"/>
            <p class="health">health: <b> ${health} </b></p>
            <div class="dice-container">${diceHtml}</div>
         </div>`
   }
}

document.getElementById("attack-button").addEventListener("click", ()=>{
   renderCharacters(players)
})

renderCharacters(players)  
